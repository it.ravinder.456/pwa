import React from "react";
import Main from "./components/Main";
import 'semantic-ui-css/semantic.min.css';

function App() {
  return( 
  <React.Fragment>
    <Main />
  </React.Fragment>
  )
}

export default App;
