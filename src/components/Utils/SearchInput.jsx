import React from "react";
import { Input } from "semantic-ui-react";

const SearchInput = (props) => (
  <Input
    loading={props.loading}
    icon={props.icon ? props.icon : "search"}
    error={props.error ? true : false}
    placeholder="Search..."
    onChange={props.onChange}
    autoFocus
  />
);

export default SearchInput;
