/**
 *
 * @param {*array of objects} array
 * @param {*the property name to be taken to count}
 */
export function getTotalCountOfSpecifiedValueFromArray(array, propertyName) {
  var count = 0;
  array.map((value, index) => {
    count = count + Number(value[propertyName]);
  });
  return count;
}

/**
 *  Global Search is useful to search string and numbers over all ARRAY of OBJECTS
 *  search string is not null
 */
export function globalSearch(data, searchString = "") {
  if (searchString != "" && data.length > 0) {
    let filtered = data.filter((entry) =>
      Object.values(entry).some(
        (val) =>
          (typeof val !== "string" && val !== null
            ? (val = val.toString())
            : val) && val.toLowerCase().includes(searchString.toLowerCase())
      )
    );
    return filtered;
  }
  return data;
}

// export function getPageSizedItems(pageSize,data) 
