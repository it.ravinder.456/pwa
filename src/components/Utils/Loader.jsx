import React from "react";
import { Segment, Dimmer, Image } from "semantic-ui-react";


// #region constants

// #endregion

// #region styled-components

// #endregion

// #region functions

// #endregion

// #region component
const propTypes = {};

const defaultProps = {};

/**
 *
 */
const Loader = (porps) => {
  return (
    <React.Fragment>
      <Segment>
        <Dimmer active inverted>
          <Loader inverted>Loading</Loader>
        </Dimmer>
        {/* <Image src="https://react.semantic-ui.com/images/wireframe/short-paragraph.png" /> */}
      </Segment>
    </React.Fragment>
  );
};


export default Loader;
