import React from 'react'
import {  Dropdown } from 'semantic-ui-react'

const options = [
  { key: '10', value: '10', text: '10' },
  { key: '25', value: '25', text: '25' },
  { key: '50', value: '50', text: '50' },
  { key: '100', value: '100', text: '100' },
 
]

const SingleSelectDropdown = (props) => (
  <Dropdown 
  placeholder={props.placeholder ?props.placeholder : ""} 
  options={props.options ? props.options : options} 
  value={props.value ? props.value : "10"}
//   search 
  selection
  />
)

export default SingleSelectDropdown