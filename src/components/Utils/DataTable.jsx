import React, { Component } from "react";
import SearchInput from "./SearchInput";
import { getTotalCountOfSpecifiedValueFromArray, globalSearch } from "./Helpers";
import { Icon } from "semantic-ui-react";
import SingleSelectDropdown from "./SingleSelectDropdown";


export class DataTable extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      data: {},
      SearchInput:"",
    };
  }
  componentDidMount() {
    console.log("child componentDidMount");
  }
  componentDidUpdate(prevProps, prevState) {
    console.log("child componentdidupdate");
    if (prevState.data !== prevState.data) {
      this.setState({ data: prevState.data });
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    console.log("child getDerivedStateFromProps");
    if (nextProps.data !== prevState.data) {
      return { data: nextProps.data };
    } else return null;
  }

  handleChange = (e) => {
    console.log("value", e.target.value);
    this.setState({ loading: true,SearchInput:e.target.value });
    setTimeout(() => {
      this.setState({ loading: false });
    }, 1000);
  };


  render() {
    console.log("child render");
    let data=this.state.data;
    if(this.state.SearchInput!==""){
        data=globalSearch(data,this.state.SearchInput);
    }
    let StatesWiseJSX = Object.entries(data).map((value, index) => {
      if (value[1].statecode === "UN") {
        return null;
      }
      return (
        <tr>
          <td>{value[0]}</td>
          <td>
            {getTotalCountOfSpecifiedValueFromArray(
              Object.values(value[1].districtData),
              "active"
            )}
          </td>
          <td>
            {getTotalCountOfSpecifiedValueFromArray(
              Object.values(value[1].districtData),
              "confirmed"
            )}
          </td>
          <td>
            {getTotalCountOfSpecifiedValueFromArray(
              Object.values(value[1].districtData),
              "recovered"
            )}
          </td>
          <td>
            <Icon name="info circle cursor-pointer" />
          </td>
        </tr>
      );
    });
    return (
      <React.Fragment>
               

        <div
          className="panel panel-default my-sm-4"
        >
          <div className="panel-heading no-color flex">
            <p className="panel-title mt-2">Covid Updates</p>
            <div className="pull-right ml-2">
              <SearchInput
                loading={this.state.loading}
                onChange={this.handleChange}
              />
             
            </div>
            <div className="width10 ml-2">
            <SingleSelectDropdown 
              />
            </div>
          </div>
          <div className="panel-body">
            <table className="table table-bordered mb-0">
              <tbody>
                <tr className="table-heading-bg">
                  <th>State</th>
                  <th>Active</th>
                  <th>Confirmed</th>
                  <th>Recovered</th>
                  <th>Actions</th>
                </tr>
              </tbody>
              <tbody>
                {StatesWiseJSX}
              </tbody>
            </table>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default DataTable;
