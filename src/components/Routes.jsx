import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import DashBoard from "./dashboard/DashBoard";
import NotFound from "./handlers/NotFound";

export class Routes extends Component {
  render() {
    return (
      <React.Fragment>
        <Router>
          <Switch>
            <Route exact path="/" component={DashBoard} />
            <Route exact path="*" component={NotFound} />
          </Switch>
        </Router>
      </React.Fragment>
    );
  }
}
export default Routes;


