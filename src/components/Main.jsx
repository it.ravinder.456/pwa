import React, { Component } from "react";
import Header from "./main/Header";
import LeftSideBar from "./main/LeftSideBar";
import Footer from "./main/Footer";
import DashBoard from "./dashboard/DashBoard";
import Routes from "./Routes";

export class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sidebarToggle: false,
    };
  }

  handleSidebar = () => {
    this.setState({ sidebarToggle: !this.state.sidebarToggle });
  };

  render() {
    return (
      <div
        className={`sb-nav-fixed ${
          this.state.sidebarToggle ? "sb-sidenav-toggled" : ""
        }`}
      >
        <Header handleSidebar={this.handleSidebar} />
        <div id="layoutSidenav">
          <LeftSideBar />
          <div id="layoutSidenav_content">
            <Routes />
            <Footer />
          </div>
        </div>
      </div>
    );
  }
}

export default Dashboard;
