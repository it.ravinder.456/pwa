import React, { Component } from "react";
import DataTable from "../Utils/DataTable";
import { postServiceCALLS, getServiceCALLS } from "../main/ServiceCalls";
// import DashBoardRoutes from "./DashBoardRoutes";
// import { Link } from "react-router-dom";

export class DashBoard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      covid_master_data: {},
    };
  }

  async componentDidMount() {
    console.log("parent componentDidMount");
    let covid_master_data = await getServiceCALLS(
      "state_district_wise.json",
      "https://api.covid19india.org"
    );
    this.setState({ covid_master_data });
  }
  render() {
    console.log("parent render");
    // console.log("DashBoard", this.state);
    document.title = "Dashboard";
    return (
      <React.Fragment>
        <main>
          <div className="container-fluid">
            <h1 className="mt-4">Dashboard</h1>
            <ol className="breadcrumb mb-4">
              <li className="breadcrumb-item active">Dashboard</li>
            </ol>
            <div className="row">
              <div className="col-xl-3 col-md-6">
                <div className="card bg-primary text-white mb-4">
                  <div className="card-body">Employees</div>
                  <div className="card-footer d-flex align-items-center justify-content-between">
                    <a className="small text-white stretched-link" href="#">
                      View Details
                    </a>
                    <div className="small text-white">
                      <i className="fa fa-angle-right"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-xl-3 col-md-6">
                <div className="card bg-warning text-white mb-4">
                  <div className="card-body">Weather Report</div>
                  <div className="card-footer d-flex align-items-center justify-content-between">
                    <a className="small text-white stretched-link" href="#">
                      View Details
                    </a>
                    <div className="small text-white">
                      <i className="fa fa-angle-right"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-xl-3 col-md-6">
                <div className="card bg-success text-white mb-4">
                  <div className="card-body">Countries</div>
                  <div className="card-footer d-flex align-items-center justify-content-between">
                    <a className="small text-white stretched-link" href="#">
                      View Details
                    </a>
                    <div className="small text-white">
                      <i className="fa fa-angle-right"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-xl-3 col-md-6">
             {/* <Link to={`${this.props.match.path}/covidupdates`}>  */}

                <div className="card bg-danger text-white mb-4">
                  <div className="card-body">Covid Updates</div>
                  <div className="card-footer d-flex align-items-center justify-content-between">
                    <a className="small text-white stretched-link" href="#">
                      View Details
                    </a>
                    <div className="small text-white">
                      <i className="fa fa-angle-right"></i>
                    </div>
                  </div>
                </div>
              {/* </Link> */}

              </div>
            </div>
            {/* <DashBoardRoutes /> */}
            <DataTable data={this.state.covid_master_data} />
          </div>
        </main>
      </React.Fragment>
    );
  }
}

export default DashBoard;
