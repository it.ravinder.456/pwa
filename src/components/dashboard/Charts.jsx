import React, { Component } from "react";

export class Charts extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-xl-6">
            <div className="card mb-4">
              <div className="card-header">
                <i className="fa fa-chart-area mr-1"></i>Area Chart Example
              </div>
              <div className="card-body">
                <canvas id="myAreaChart" width="100%" height="40"></canvas>
              </div>
            </div>
          </div>
          <div className="col-xl-6">
            <div className="card mb-4">
              <div className="card-header">
                <i className="fa fa-chart-bar mr-1"></i>Bar Chart Example
              </div>
              <div className="card-body">
                <canvas id="myBarChart" width="100%" height="40"></canvas>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Charts;
