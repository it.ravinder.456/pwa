import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import DataTable from "../Utils/DataTable";

export class DashBoardRoutes extends Component {
  render() {
    var ROOT_PATH = "/dashboard";
    return (
      <React.Fragment>
        <Router>
          <Switch>
            <Route path={ROOT_PATH + "/covid#updates"} component={DataTable} />
          </Switch>
        </Router>
      </React.Fragment>
    );
  }
}

export default DashBoardRoutes;
