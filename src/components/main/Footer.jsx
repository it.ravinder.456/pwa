import React, { Component } from "react";
import { Link } from "react-router-dom";

export class Footer extends Component {
  render() {
    return (
      <React.Fragment>
        <footer className="py-4 bg-light mt-auto">
          <div className="container-fluid">
            <div className="d-flex align-items-center justify-content-between small">
              <div className="text-muted">
                Copyright &copy; Your Website 2019
              </div>
              <div>
                <Link to="/">
                  <a>Privacy Policy</a>
                </Link>
                &middot;
                <Link to="/">
                  <a>Terms &amp; Conditions</a>
                </Link>
              </div>
            </div>
          </div>
        </footer>
      </React.Fragment>
    );
  }
}

export default Footer;
