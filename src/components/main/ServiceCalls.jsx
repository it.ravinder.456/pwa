import axios from "axios";
import _ from "lodash";

// Get all getbased service calls.
export async function getServiceCALLS(EndPoint, hostName = "localhost:5000") {
  const prepareURL = hostName + "/" + EndPoint;
  var tempResponseObject = {};
  console.log("prepareURL =", prepareURL);
  tempResponseObject = await axios.get(prepareURL).then((response) => {
    console.log("response", response);
    if (response.status === 200 && response.data) {
      return response.data;
    } else {
      return response;
    }
  });
  return tempResponseObject;
}

// Get all post based service calls.
export async function postServiceCALLS(
  EndPoint,
  dataObject = {},
  hostName = "localhost:5000"
) {
  const prepareURL = hostName + "/" + EndPoint;
  console.log("prepareURL =", prepareURL, "dataObject=", dataObject);
  await axios.post(prepareURL, dataObject).then((response) => {
    console.log("API Response Object (Post Service Call)=", response);
    if (response.status === 200) {
      const responsedata = response.data;
      responsedata = 200;
      return responsedata;
    } else {
      return response;
    }
  });
}
