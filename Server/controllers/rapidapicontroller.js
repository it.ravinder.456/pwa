const axios = require("axios");
var API_KEY="b9df61cf54msh14e9dfa1494c63fp1fe3c4jsneb9001f053e5";
var date = new Date();
date.setDate(date.getDate() - 2);
date = new Date(date).toISOString().slice(0, 10);

exports.getcovidupdatestotal = (req, res, next) => {
  axios({
    method: req.method,
    url: "https://covid-19-data.p.rapidapi.com/totals",
    headers: {
      "content-type": "application/octet-stream",
      "x-rapidapi-host": "covid-19-data.p.rapidapi.com",
      "x-rapidapi-key": API_KEY,
      useQueryString: true,
    },
    params: {
      format: "json",
    },
  })
    .then((response) => {
    //   console.log(response);
      res.send(response.data);
    })
    .catch((error) => {
    //   console.log(error);
      res.send({error:error.message});
    });
};

exports.getcovidupdatesReportByCountry = (req, res, next) => {

  axios({
    method: "GET",
    url: "https://covid-19-data.p.rapidapi.com/report/country/name",
    headers: {
      "content-type": "application/octet-stream",
      "x-rapidapi-host": "covid-19-data.p.rapidapi.com",
      "x-rapidapi-key": API_KEY,
      useQueryString: true,
    },
    params: {
      "date-format": "YYYY-MM-DD hh:mm:ss",
      format: "json",
      date: date,
      name: req.body.Country,
    },
  })
    .then((response) => {
    //   console.log(response);
      res.send(response.data);
    })
    .catch((error) => {
    //   console.log(error);
      res.send({error:error.message});
    });
};

exports.getallcountries = (req, res, next) => {
  axios({
    "method":"GET",
    "url":"https://covid-19-data.p.rapidapi.com/help/countries",
    "headers":{
    "content-type":"application/octet-stream",
    "x-rapidapi-host":"covid-19-data.p.rapidapi.com",
    "x-rapidapi-key":"b9df61cf54msh14e9dfa1494c63fp1fe3c4jsneb9001f053e5",
    "useQueryString":true
    },"params":{
    "format":"json"
    }
  })
    .then((response) => {
    //   console.log(response);
      res.send(response.data);
    })
    .catch((error) => {
    //   console.log(error);
      res.send({error:error.message});
    });
};
