const axios = require("axios");
const express = require("express");
const router = express.Router();
var rapidapicontroller=require("../controllers/rapidapicontroller")

router.get("/GetCovidTotals",rapidapicontroller.getcovidupdatestotal);
router.get("/GetCovidReportsByCountry",rapidapicontroller.getcovidupdatesReportByCountry);
router.get("/getAllCountries",rapidapicontroller.getallcountries);

module.exports = router;
